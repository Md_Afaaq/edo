================
Status Fetch API
================

.. role:: underline
  :class: underline

This method is used to get Status update of the transaction by the EDO client using the
transaction id of the request in Async mode. If the transaction is in synchronous mode, then
there is no need to call this API request.

-------
Request
-------


.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Method
     - URL
   * - ``POST``
     - <webapi-url>/statusfetch/

--------------
Request Format
--------------

The request is encrypted with client key(specified to each Application at EDO Switch
GUI level) and the request is decrypted at EDO Switch API.

.. code-block:: Json

    {"txnid":"xxxxxx","slk":"xxxxxx"}

.. list-table::
   :widths: 1 2 3
   :header-rows: 1

   * - Attribute
     - Type
     - Description
    
   * - txnid
     - string
     - Transaction ID of the request.
     
   * - Slk
     - string
     - Licence Key

--------
Response
--------

The response from EDO Switch API is encrypted with the client key (specified to
each Application at EDO Switch GUI level) and the response is decrypted at EDO
client. Response with respcode 100, the request has to be re-scanned / re-masked from
the edo client.

+------------+---------------------------------------------------------+
| Status     | Response                                                |
+============+=========================================================+
| ``200``    | **Example response:-**                                  |
|            |                                                         |
|            | .. code-block:: Json                                    |
|            |                                                         |
|            |    {"respcode":"200","respdesc":"File Ready and please  |
|            |     download the file within 24 hrs.","txnid":"xxxx",   |
|            |     "file":"Url of the Masked File.","txnmode":"0/1",   |
|            |     "timespan":"X sec"}                                 |
+------------+---------------------------------------------------------+
| For more error code please refer Annexure.                           |                    
+----------------------------------------------------------------------+

--------
Annexure
--------

.. list-table::
   :widths: 1 2 
   :header-rows: 1

   * - Error Code
     - Code Description
    
   * - 400
     - request is Missing.
     
   * - 401
     - request Format error.

   * - 402
     - request Parsing error.

   * - 403
     - Invalid User ID or Password.

   * - 404
     - Invalid AppCode.

   * - 405
     - User Id is Missing.

   * - 406
     - Password is Missing.

   * - 407
     - AppCode is Missing. 

   * - 408
     - User Id is not Active.

   * - 409
     - AppCode is not associated to this User Id.

   * - 410
     - Mail Id is not exists for this User Id.

   * - 411
     - Request Schema Validation Failed.

   * - 412
     - SLK is Missing.

   * - 413
     - rrn is Missing.

   * - 414
     - Uid is Missing.

   * - 415
     - Sertype is Missing.

   * - 416
     - Input File is Missing.

   * - 417
     - Txn mode is Missing.

   * - 418
     - Invalid Slk (Licence Key).

   * - 419
     - SLK is Inactive.

   * - 420
     - SLK is expired.

   * - 421
     - Invalid Service type.

   * - 422
     - Service type is Inactive.

   * - 423
     - Invalid TxnMode

   * - 424
     - Invalid Uid.

   * - 425
     - Invalid File data.

   * - 426
     - TxnId is Missing.

   * - 427
     - Invalid TxnId.

   * - 428
     - Retry status Fetch after specified Timespan.

   * - 429
     - TxnId is processed in Sync mode and response is already received.

   * - 430
     - File is expired (>24 hrs) and deleted. Please Re-Mask the file.

   * - 431
     - AppCode is Inactive.

   * - 432
     - Invalid UserID.

   * - 433
     - Invalid User Account.

   * - 434
     - User Account is not Active.

   * - 435
     - Application does not have the Licence Key.

   * - 436
     - Transaction is processed and status id fetched for maximum attempts.

   * - 437
     - UserId does not have registered email.

   * - 438
     - Invalid Request.

   * - 439
     - Invalid API Key.

   * - 440
     - Invalid File type for base64 file.

   * - 441
     - Invalid File Size as per the limits.

   * - 442
     - No. of pages in pdf file exceeds (> 50) the limit.

   * - 443
     - Input File Validation is Failed.

   * - 444
     - Invalid File Resolution.

   * - 500
     - Something went wrong. Please try again later.

   * - EDO900
     - AUA Request Timed Out.

   * - 434
     - User Account is not Active.

   * - EDO <HttpStautsCode>
     - Internal Error in EDO Core Server Connectivity.

   * - EDO500
     - | Something went Wrong!! Please try again. 
       | (Internal Error in EDO Switch Server).

