# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'EDO Switch API'
copyright = '2021, Syntizen'
author = 'Syntizen'
release = '1.0'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.todo', 'sphinx.ext.viewcode', 'sphinx.ext.autodoc']

intersphinx_mapping = {'python': ('file:///home/encoding/Desktop/docs/_build/html/index.html', None)}

# base_url = 'file:///home/encoding/Desktop/docs/_build/html/index.html'
# version_choices = [
#                 ('latest', '2.1'),
#                 ('2.0.2', '2.0'),
#                 ('1.3.3', '1.3')]
# version_choices = [base_url + v[0], v[1] for v in version_choices]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
version = '1.0'
intersphinx_mapping = {
    'rtd': ('file:///home/encoding/Desktop/docs/_build/html/index.html', None),
    'new': ('file:///home/encoding/Desktop/docs/_build/html/index.html', None),

    'sphinx': ('https://www.sphinx-doc.org/en/stable/', None),
}

# intersphinx_mapping = {
#     'otherbook':
#         ('https://myproj.readthedocs.io/projects/otherbook/en/latest',
#             ('../../otherbook/build/html/objects.inv', None)),
# }

html_theme = 'sphinx_rtd_theme'
html_theme_path = ["_theme"]
html_theme_options = {'display_version': True,}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']