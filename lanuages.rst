=================================
EDO Switch API v2.0 - Integration
=================================

Sample code snippets for EDO Switch API into different programming languages like curl, go, java, JavaScript, NodeJS etc.

Please find below points while integrating the EDO Switch API v2.0.

| 1.) All the input requests are validated mandatory using an **‘apikey’** sent in therequest headers.
| 2.) **‘apikey’** is may be any string value of length 32. Sample apikey’s
|    a.) DC8B71BC3AB56DF8C90C8678B08A2BE8
|    b.) 0F2E4B2C8504F5DB893F56AEDB7B1B5C
|    c.) 8BEFA74182DA6A5C5A6D69A60F942000
| 3.) All the input requests are encrypted with this **‘apikey’** in **AES-128 encryption / decryption**
|    algorithm.


.. 4. Sample Encrypt-Decrypt Methods in java and .Net as follows.
.. a) C# Utility

.. There are many programming langages and process to make HTTP call are different in each language. So, Here you can find some language code snippets for ``GET`` API requests.

* CURL
* JAVA
* NodeJS
* PHP
* Python
* Ruby


CRUL
****

.. code-block:: python
   :emphasize-lines: 1

   curl --request [your_api_endpoint]

Java
****

.. .. literalinclude:: ase.java
..    :language: Java

.. code-block:: Java

   class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!"); 
    }
   }   


Python
******

.. literalinclude:: ase.py
   :language: python

PHP
***

.. code-block:: PHP

   <?php
   echo "Hello World!";
   ?>

Ruby
****

.. code-block:: ruby

   puts "Hello World!"