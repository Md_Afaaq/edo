===============
ECO Connect API
===============

This API is used to access various methods/services which are available in the EDO
Switch system. The major activities like EDO Connect Mask API, Status Fetch API,
etc.. are handled in this API. This document has all the methods with input and
output parameters in detail which help in access of the API.

This method is the client interface to perform the file masking functionality in the EDO Switch.

-------
Request
-------

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - Method
     - URL
   * - POST
     - <webapi-url>/edoconnect/


.. list-table::
   :widths: 25 25 49
   :header-rows: 1

   * - Type 
     - Params
     - Type
   * - POST
     - requestjson
     - Json string

--------------
Request Format
--------------

The request is encrypted with client key(specified to each Application at EDO Switch
GUI level) and the request is decrypted at EDO Switch API.

.. code-block:: Json

    {"uid":"","slk":"","rrn":"","env":"","lat":"","lon":"","devid":"","refr":"","sertype":"",
    "file":"","ver":"","txnmode":"0/1"}

.. list-table::
   :header-rows: 1
   :widths: 1 2 3


   * - Attribute 
     - Type
     - Description
   * - uid 
     - string
     - Userid, this will be shared by the
       Service provider.
   * - slk 
     - string
     - | Valid Licence key of User Id to perform the EDO transactions, this will
       | be shared by the Service provider.
   * - rrn
     - string
     - | Request Reference Number Generated at Client end as reference to the
       | transaction between EDO Client & EDO Switch API. It should be
       | less than 12 characters in length.

   * - env
     - string
     - | Environment Value
       | 1- For Android
       | 2- For API
       | 3- For Desktop
       | 4- For Web
       
   * - lat 
     - string
     - Lattitude of the device location

   * - lon
     - string
     - Lattitude of the device location
     
   * - ["" | devid]
     - string
     - Device ID/Mac Address. It is optional for few cases.     

   * - refr
     - string
     - | Any refernece that is sent in the request by the client for the
       | transaction reference.

   * - sertype
     - string
     - | Service Type defined in the EDO Switch:
       | EDO Diagnoser : 01
       | EDO Scan : 02

   * - file
     - string
     - Base64 of the Image/Pdf file to be masked.

   * - ver 
     - string
     - | EDO Client version and it is as below,
       | EDO Scan - Exe version
       | Android - Android APK version 2.0
       | IOS - IOS Apk version 2.0
       | EDO Connect API - 2.0

   * - txmode 
     - string
     - | Based on transaction processing type, the value of txnmode is 
       | defined as below.
       | Async(Background Process)- 1
       | Sync(On the Fly API call) - 0

--------
Response
--------

The response from EDO Switch API is encrypted with the client key (specified to
each Application at EDO Switch GUI level) and the response is decrypted at EDO
client. Responses with respcode 100 & 500 the request has to be retried from the edo
client.


+------------+---------------------------------------------------------+
| Status     | Response                                                |
+============+=========================================================+
| ``200``    | **Example response:-**                                  |
|            |                                                         |
|            | .. code-block:: Json                                    |
|            |                                                         |
|            |    {"respcode":"200","respdesc":"success.","txnid":"    |
|            |     xxxx","rrn":"yyyy","refr":"zzzz","file":"base64 of  |
|            |     masked file","txnmode":"0/1","timespan":"X sec"}    |
|            |                                                         |
|            |                                                         |
|            | | Based on TxnMode the success response is varied.      |
|            | | **If TxnMode is 0(Sync):**                            |
|            | | Base64 of masked file is sent in the response.        |
|            | | **If TxnMode is 1(ASync):**                           |
|            | | Timespan along with TxnID is sent in response so that |
|            | | the EDO Client can fetch the status of the Transaction| 
|            | | after the mentioned Timespan in the response from the |
|            |   Request initiated timestamp.                          |
|            |                                                         | 
|            |                                                         | 
|            |                                                         |
+------------+---------------------------------------------------------+
| For more error code please refer Annexure.                           |                    
+----------------------------------------------------------------------+


