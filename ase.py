# -*- coding: utf-8 -*-

import sys
import base64
from hashlib import md5
from Crypto import Random
from Crypto.Cipher import AES

py2 = sys.version_info[0] == 2

BLOCK_SIZE = 16
KEY_LEN = 32
IV_LEN = 16

KEY = 'f379e0b661ae4650b19169e4d93665dc'

def encrypt(raw, passphrase=KEY):
    """
    Encrypt text with the passphrase
    @param raw: string Text to encrypt
    @param passphrase: string Passphrase
    @type raw: string
    @type passphrase: string
    @rtype: string
    """
    salt = Random.new().read(8)
    key, iv = __derive_key_and_iv(passphrase, salt)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(b'Salted__' + salt + cipher.encrypt(__pkcs7_padding(raw))).decode('utf-8')

def decrypt(enc, passphrase=KEY):
    """
    Decrypt encrypted text with the passphrase
    @param enc: string Text to decrypt
    @param passphrase: string Passphrase
    @type enc: string
    @type passphrase: string
    @rtype: string
    """
    ct = base64.b64decode(enc)
    salted = ct[:8]
    if salted != b'Salted__':
        return ""
    salt = ct[8:16]
    key, iv = __derive_key_and_iv(passphrase, salt)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return __pkcs7_trimming(cipher.decrypt(ct[16:]))

def __pkcs7_padding(s):
    """
    Padding to blocksize according to PKCS #7
    calculates the number of missing chars to BLOCK_SIZE and pads with
    ord(number of missing chars)
    @param s: string Text to pad
    @type s: string
    @rtype: string
    """
    s_len = len(s if py2 else s.encode('utf-8'))
    s = s + (BLOCK_SIZE - s_len % BLOCK_SIZE) * chr(BLOCK_SIZE - s_len % BLOCK_SIZE)
    return s if py2 else bytes(s, 'utf-8')

def __pkcs7_trimming(s):
    """
    Trimming according to PKCS #7
    @param s: string Text to unpad
    @type s: string
    @rtype: string
    """
    if sys.version_info[0] == 2:
        return s[0:-ord(s[-1])]
    return s[0:-s[-1]]

def __derive_key_and_iv(password, salt):
    """
    Derive key and iv
    @param password: string Password
    @param salt: string Salt
    @type password: string
    @type salt: string
    @rtype: string
    """
    d = d_i = b''
    enc_pass = password if py2 else password.encode('utf-8')
    while len(d) < KEY_LEN + IV_LEN:
        d_i = md5(d_i + enc_pass + salt).digest()
        d += d_i
    return d[:KEY_LEN], d[KEY_LEN:KEY_LEN + IV_LEN]


if __name__ == '__main__':    # code to execute if called from command-line
    ci = encrypt("sample text to encrypt", "f379e0b661ae4650b19169e4d93665dc")
    print(ci)
    print(decrypt('U2FsdGVkX1/+5k/NlWPkSSA6lqlehku3izxYPEn4BQH+IylVjjmMBsJ8YBNotV9x', "f379e0b661ae4650b19169e4d93665dc"))