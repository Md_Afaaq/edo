.. edo_switch_api documentation master file, created by
   sphinx-quickstart on Fri Mar 26 10:24:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to EDO Switch API's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Introduction:

   Introduction 
   lanuages


.. toctree::
   :maxdepth: 2
   :caption: EDO Connect API:

   edoconnect

.. toctree::
   :maxdepth: 2
   :caption: Status Fetch API:

   StatusFetchAPI


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
